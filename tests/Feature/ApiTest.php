<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ApiTest extends TestCase
{
    use RefreshDatabase;

    public function testShouldCreateTransaction()
    {
        $user1 = User::factory()->create(['balance' => 100]);
        $user2 = User::factory()->create(['balance' => 100]);
        $this->post('/api/create', [
            'from_user_id' => $user1->id,
            'to_user_id' => $user2->id,
            'amount' => 20
        ])
        ->assertOk();

        $this->assertDatabaseHas('users', ['id' => $user1->id, 'balance' => 80]);
        $this->assertDatabaseHas('users', ['id' => $user2->id, 'balance' => 120]);
        $this->assertDatabaseHas('transactions', ['from_user_id' => $user1->id, 'to_user_id' => $user2->id, 'amount' => 20]);
    }

    public function testShouldNotCreateTransactionInsufficientBalance()
    {
        $user1 = User::factory()->create(['balance' => 10]);
        $user2 = User::factory()->create(['balance' => 100]);
        $this->post('/api/create', [
            'from_user_id' => $user1->id,
            'to_user_id' => $user2->id,
            'amount' => 20
        ])
            ->assertStatus(302);

        $this->assertDatabaseHas('users', ['id' => $user1->id, 'balance' => 10]);
        $this->assertDatabaseHas('users', ['id' => $user2->id, 'balance' => 100]);
        $this->assertDatabaseMissing('transactions', ['from_user_id' => $user1->id, 'to_user_id' => $user2->id, 'amount' => 20]);
    }

    public function testShouldNotCreateTransactionNotSearchUser()
    {
        $user = User::factory()->create(['balance' => 100]);
        $this->post('/api/create', [
            'from_user_id' => '100',
            'to_user_id' => $user->id,
            'amount' => 20
        ])
            ->assertStatus(302);

        $this->assertDatabaseHas('users', ['id' => $user->id, 'balance' => 100]);
        $this->assertDatabaseMissing('users', ['id' => '100']);
        $this->assertDatabaseMissing('transactions', ['from_user_id' => '100', 'to_user_id' => $user->id]);
    }
}
