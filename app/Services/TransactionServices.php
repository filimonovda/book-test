<?php declare(strict_types=1);

namespace App\Services;

use App\Models\Transaction;
use App\Models\User;

class TransactionServices
{
    public function batch(int $fromUserId, int $toUserId, float $amount): bool
    {
        if (
            User::query()->find($fromUserId)->increment('balance', -$amount) &&
            User::query()->find($toUserId)->increment('balance', $amount)
        ) {
            return (bool)Transaction::query()->create([
                'from_user_id' => $fromUserId,
                'to_user_id' => $toUserId,
                'amount' => $amount
            ]);
        }
        return false;
    }
}
