<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = ['from_user_id', 'to_user_id', 'amount'];

    public function rules(): array
    {
        return [
            'from_user_id' => 'required|exists:users',
            'to_user_id' => 'required|exists:users',
            'amount' => 'required|min:0',
        ];
    }
}
