<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\CreateTransactionRequest;
use App\Http\Requests\ShowTransactionRequest;
use App\Services\TransactionServices;
use Illuminate\Http\Response;

class TransactionController extends Controller
{
    private TransactionServices $transactionServices;

    public function __construct(TransactionServices $transactionServices)
    {
        $this->transactionServices = $transactionServices;
    }

    public function create(CreateTransactionRequest $request): Response
    {
        return $this->transactionServices->batch(
            (int)$request->get('from_user_id'),
            (int)$request->get('to_user_id'),
            (float)$request->get('amount')
        ) ? response(true) : response(false, 302);
    }

    public function show(ShowTransactionRequest $transaction): Response
    {
        //
    }
}
