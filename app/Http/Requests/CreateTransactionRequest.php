<?php declare(strict_types=1);

namespace App\Http\Requests;

use App\Rules\SufficientBalance;
use Illuminate\Foundation\Http\FormRequest;


class CreateTransactionRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'from_user_id' => ['required', 'exists:App\Models\User,id', new SufficientBalance()],
            'to_user_id' => ['required', 'exists:App\Models\User,id'],
            'amount' => ['required', 'min:0'],
        ];
    }

    public function messages()
    {
        return [];
    }
}
