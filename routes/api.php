<?php

use App\Http\Controllers\TransactionController;
use Illuminate\Support\Facades\Route;

Route::post('create', [TransactionController::class, 'create'])->name('create');
Route::post('show', [TransactionController::class, 'show'])->name('show');
